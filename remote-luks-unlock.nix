{ config, lib, ... }:
with lib;
let
  cfg = config.remoteLuksUnlock;
in
{
  options.remoteLuksUnlock = {
    enable = mkEnableOption "remote Luks Unlock over SSH";
    authorizedKeys = mkOption {
      type = types.listOf types.str;
      description = "List of keys allowed to connect via ssh to the root user to unlock the Luks volume";
    };
    hostKeys = mkOption {
      type = types.listOf types.str;
      description = "List of host keys for ssh server in initrd";
    };
    luksVolume = mkOption {
	  type = types.str;
	  description = "the path of the volume to unlock";
    };
    ssh.port = mkOption {
	  type = types.int;
	  description = "the ssh port to listen to";
    };
  };
  config = mkIf cfg.enable {
	  boot.initrd = {
	    network  = {
	      enable = true;
	      ssh = {
	        enable = true;
	        port = cfg.ssh.port;
	        hostKeys = cfg.hostKeys;
	        authorizedKeys = cfg.authorizedKeys;
	      };
	      postCommands = ''
	      echo 'cryptsetup open ${cfg.luksVolume} cryptedroot && echo > /tmp/continue' >> /root/.profile
	      echo 'starting sshd ...'
	      '';
	    };
	    postDeviceCommands = ''
	    echo 'waiting for root device to be opened ...'
	    mkfifo /tmp/continue
	    cat /tmp/continue
	    '';
	    luks.forceLuksSupportInInitrd = true;
	  };
  };
}
