{
    inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";

    outputs = {
        self,
        nixpkgs,
        ...
    } : {
        nixosModules = {
            remoteLuksUnlock = import ./remote-luks-unlock.nix;
       };
       nixosModule = self.nixosModules.remoteLuksUnlock;
    };
}
